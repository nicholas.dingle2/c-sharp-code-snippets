﻿/* Made by: Nicholas Dingle
 * Last modified: 31/3/2019
 * 
 **********************READ THIS FIRST***********************
 * This program has been written to contain snippets of code 
 * demonstrating how to perform different functions in C#.
 * 
 * See line 21 (underneath static void Main) for instructions.
 * 
 * REMEMBER: Anything written in green is a comment from me 
 * to you and can be ignored/deleted.
 ************************************************************/
using System;

namespace Code_Snippet_Bin
{
    class Program
    {
        static void Main(string[] args)
        {
            /* EXPLANATION
             * 
             * The following commands are written in separate files.
             * All code files are stored in the Code folder.
             * 
             * e.g. ConsoleCommands.Run() = Code\ConsoleCommands.cs
             * 
             * Remove the comment symbol (//) in front of a line
             * to enable that code. Insert the comment symbol (//)
             * in front of a line to disable that code.
             * 
             * Right click on a Run() command and click "Go to 
             * definition" to see the code that is executed (line
             * of code must be uncommented first).
             * ****************************************************/

            // Term 1 code concepts
            ConsoleCommands.Run();
            //Variables.Run();
            //UserInput.Run();
            //IfStatements.Run();
            //Loops.Run();
            //Strings.Run();
            //Lists.Run();
            //Structs.Run();
            //TextFiles.Run();
            

            // Term 2 code concepts
            //Arrays.Run();


            // Console.ReadLine is used here to pause the program
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class Loops
    {

        /// <summary>
        /// Loops are all about repeating your code. Meaning you can write
        /// a block of code once and then repeat it as many times as needed.
        ///  TODO: Think about adding foreach loops
        /// </summary>
        public static void Run()
        {

            #region "1) While loop"

            // while () loops take a condition (like a if statement) after the
            // keyword. The block of code is repeated so long as the condition 
            // stays true.
            //
            // number++ here increases its value by one
            int number = 1;
            while (number <= 10)
            {
                Console.WriteLine($"Number is currently at {number}");
                number++;
            }

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "2) Do while loop"

            // do { } while () loops take a condition at the bottom and the code
            // block is repeated so long as the condition stays false.
            int userNumber;
            int total = 0;
            Console.WriteLine("I'm going to add all your numbers together.");

            do
            {
                Console.Write("Enter a number (enter 0 to stop adding): ");
                userNumber = int.Parse(Console.ReadLine());
                total += userNumber;
            } while (userNumber != 0);

            Console.WriteLine($"Your total is {total}");

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "3) For loops"

            // for (;;) loops repeat a set number of times and are useful when
            // you know how many times your code might repeat.
            // A for loop condition has three parts (separated by semi-colons).
            // e.g. for (int i = 0; i < 10; i++)
            //    1) int i = 0; variable init
            //    2) i < 10; stopping condition
            //    3) i++ variable increase/decrease
            // This means the above example would create a variable, called i,
            // it would start at 0. The loop will stop when i reaches 10. Each 
            // time the code block is executed, i is increased by 1.
            Console.WriteLine("I'm just like the top loop.");

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Number {i}");
            }

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "4) User input in loops"

            // User input can also be used to control the loop.
            // NOTE: Never trust user input and check that it won't cause errors
            Console.Write("Please enter a number to count to: ");
            int topNumber = int.Parse(Console.ReadLine());
            int counter = 0;

            while (counter <= topNumber)
            {
                Console.WriteLine(counter);
                counter++;
            }

            #endregion

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class Arrays
    {

        /// <summary>
        /// An array is a variable, but it stores more than one value at the same time.
        /// It allows for a single declaration to be made and for multiple values to
        /// be stored inside. Each value is access or modified by using its unique 
        /// number, called an index.
        /// Arrays can be of any datatype!
        /// </summary>
        public static void Run()
        {

            #region "1) Create and print an array"

            // arrays are created by using the [] brackets after a datatype. Values are
            // fed into arrays with the { } brackets and values inside.
            int[] numbers = { 5, 6, 1, 2, 9 };
            Console.WriteLine("I'm printing some numbers!");
            Console.WriteLine(numbers[0]);      // Prints 5
            Console.WriteLine(numbers[1]);      // Prints 6
            Console.WriteLine(numbers[2]);      // Prints 1
            Console.WriteLine(numbers[3]);      // Prints 2
            Console.WriteLine(numbers[4]);      // Prints 9


            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "1.5) Print array using a loop"

            // Loops are generally the best way to access data inside an array. Here
            // we loop from 0 to 4 and read the numbers out
            Console.WriteLine("I'm printing some numbers, again! Using a loop.");

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(numbers[i]);  //NOTE: i is used instead of a number
            }


            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "2) User input into an array"

            // When you create an array but you don't know what numbers you want, use
            // the following line. The number is the [] brackets tells the computer
            // how many variables to make for your array.
            int[] userNumbers = new int[5];
            Console.WriteLine("Time for you to give me some numbers.");

            for (int i = 0; i < 5; i++)
            {
                Console.Write($"Enter a number ({i}):");
                userNumbers[i] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Have them back!");

            // Print those numbers back at the user
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"Your number ({i}): {userNumbers[i]}");
            }


            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "3) Calculations on array values"

            // Use for (;;) loops to perform functions on your arrays. We are  going
            // to perform maths on the numbers the user gave to us.
            int total = 0;
            int average = 0;
            int min = 0;
            int max = 0;

            // Calculate the total
            for (int i = 0; i < 5; i++)
            {
                total += userNumbers[i];
            }

            // Calculate the average
            average = total / 5;

            // Get the smallest number
            min = userNumbers[0];
            for (int i = 1; i < 5; i++)
            {
                if (min > userNumbers[i])
                {
                    min = userNumbers[i];
                }
            }

            // Get the biggest number
            max = userNumbers[0];
            for (int i = 1; i < 5; i++)
            {
                if (max < userNumbers[i])
                {
                    max = userNumbers[i];
                }
            }

            // Print our results
            Console.WriteLine($"Total = {total}");
            Console.WriteLine($"Average = {average}");
            Console.WriteLine($"Smallest = {min}");
            Console.WriteLine($"Biggest = {max}");


            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "4) User interacts with array"

            // Not all arrays are integers. You can use anything. For this example
            // we have an array of strings (months in the year) and we'll let the 
            // user choose which one they want.
            string[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            Console.Write("Please enter a number between (1-12): ");
            int userChoice = int.Parse(Console.ReadLine());

            if (userChoice >= 1 && userChoice <= 12)
            {
                Console.WriteLine(months[userChoice]);
            }

            #endregion

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class IfStatements
    {

        /// <summary>
        /// If Statements are incredibly important. They allow the programmer
        /// to ask questions and change the code's path of execution. This 
        /// means we can allow the user (or programmer) to choose between one 
        /// of two options. 
        /// </summary>
        public static void Run()
        {

            #region "1) If statement"

            Console.Write("Please don't say memes: ");
            string word = Console.ReadLine();

            // if () line doesn't require a ; at the end. Inside the brackets
            // we ask a question (called a condition) with a yes (true) or no 
            // (false) answer. If the answer is yes, then the code will execute.
            if (word == "memes")
            {
                Console.WriteLine("Sigh...");
            }

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "2) If, else if statements"

            // if () can be extended with else if () to add more questions.
            // You can use comparison operators to check a range of numbers,
            // Includes:
            //    == equal to
            //    != not equal to
            //    > greater than
            //    >= greater than or equal to
            //    < less than
            //    <= less than or equal to
            Console.Write("Please enter a number: ");
            int number = int.Parse(Console.ReadLine());

            if (number > 0)
            {
                Console.WriteLine("Your number is positive.");
            }
            else if (number < 0)
            {
                Console.WriteLine("Your number is negative.");
            }
            else if (number == 0)
            {
                Console.WriteLine("That's zero...");
            }

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "3) Menu with if statements"

            // Menu time
            Console.WriteLine("Select your favourite food from the following:");
            Console.WriteLine();
            Console.WriteLine(" (A) Apple");
            Console.WriteLine(" (B) Banana");
            Console.WriteLine(" (C) Carrot");
            Console.WriteLine(" (D) Dinosaur");
            Console.Write("Your choice: ");
            string choice = Console.ReadLine();

            // else is used at the bottom of if statements to deal with any
            // choices that you haven't covered.
            if (choice == "a")
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Nice, apples are great.");
            }
            else if (choice == "b")
            {
                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Bananas are great on weatbix.");
            }
            else if (choice == "c")
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("I don't have an orange colour!");
            }
            else if (choice == "d")
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine("Rawr.");
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine("You goose.");
            }

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "4) Complex if statement conditions"

            // Time for more inputs
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("Please please please enter a positive number: ");
            int number1 = int.Parse(Console.ReadLine());
            Console.Write("Please please please enter another positive number: ");
            int number2 = int.Parse(Console.ReadLine());

            // if () conditions can include more than just one question
            // that creates a more complicated if statement.
            if (number1 > 0 && number2 > 0)
            {
                Console.WriteLine("Phew... thank you!");
            }
            else if (number1 < 0 && number2 > 0)
            {
                Console.WriteLine("Um, close. Fix that first number.");
            }
            else if (number1 > 0 && number2 < 0)
            {
                Console.WriteLine("Um, close. Fix that second number.");
            }
            else if (number1 < 0 && number2 < 0)
            {
                Console.WriteLine("Urgh, the worst.");
            }
            else if (number1 == 0 || number2 == 0)
            {
                Console.WriteLine("WHY DO YOU INCLUDE ZERO?!?!?!");
            }

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "5) If statements for error checking"

            // if statements can also be used for basic error handling
            Console.Write("What's your favourite colour from 0 to 15? ");
            int colour = int.Parse(Console.ReadLine());

            if (colour >= 0 && colour <= 15)
            {
                Console.BackgroundColor = (ConsoleColor)colour;
                Console.WriteLine("Nice colour!");
            }
            else
            {
                Console.WriteLine("Invalid colour choice, try again bub.");
            }

            #endregion

        }

    }
}

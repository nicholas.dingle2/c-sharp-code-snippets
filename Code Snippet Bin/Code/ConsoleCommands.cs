﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class ConsoleCommands
    {
        /// <summary>
        /// Console Commands allow you to print messages to a window's 
        /// console application. Primarily the commands are to print text
        /// change colours and pause for user input.
        /// </summary>
        public static void Run()
        {

            #region "1) Printing text"

            // Console.Writeline() is designed to prints text to the screen
            // it will write what appears in the quotes "hello" (this is 
            // known as a literal string) and then move down to the next 
            // line. 
            Console.WriteLine("A console application allows");

            // Console.Write() is the same as WriteLine, prints text to the 
            // screen, but this command doesn't move down to the next line.
            Console.Write("This is a ");
            Console.Write("complete sentence. ");
            Console.WriteLine();

            // Special characters can be printed to the screen by using the 
            // escape character (\) inside your string
            //   \" prints a quote 
            //   \n prints a newline (moves down one line)
            //   \\ prints a backslash (\)
            Console.WriteLine("This is \"someone before you said\". \\ What a time to be alive. ");

            #endregion

            #region "2) Pausing the console"

            // Console.ReadLine() will pause the program until the user
            // presses the Enter key.
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            // Console.ReadKey() will pause the program until the user
            // presses a single key.
            Console.WriteLine("Press ANY key to continue...");
            Console.ReadKey();

            #endregion

            #region "3) Change colours"

            // Console.BackgroundColor changes the background of the 
            // any text printed afterward.
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("This should have a DarkCyan background.");

            // Console.ForegroundColor changes the colour of any
            // text printed afterwards.
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("This should be green text");

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "4) Clear the console"

            // Console.Clear() removes all text on screen and fills the 
            // console in with the current background colour
            Console.Clear();
            Console.WriteLine("This is the second screen. Beautiful isn't it.");

            #endregion

            #region "5) Move console cursor"

            // Console.SetCursorPosition() allows you to move the cursor to
            // a specific position before you WriteLine/Write.
            Console.SetCursorPosition(50, 10);
            Console.WriteLine("It's warm over here, ew.");

            #endregion

        }

    }
}

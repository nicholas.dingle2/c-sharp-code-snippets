﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class Strings
    {

        /// <summary>
        /// A string is a datatype that stores collection of characters. 
        /// You can store any character at *basically* any length. Strings 
        /// are a multi use datatype, you can store:
        ///  * alphanumeric, special and non-printable characters
        ///  * words
        ///  * sentences
        ///  * whole paragraphs/pages
        ///  * usernames/passwords
        ///  * unicode characters
        /// </summary>
        public static void Run()
        {

            // Get a user string
            Console.Write("Give me a sentence: ");
            string userSentence = Console.ReadLine();

            #region "1) Literal strings"

            // A literal string is one that has a set value. There are 
            // multiple ways to create a string. 
            //    " "  = a literal string
            //    @" " = a verbatim string, no need for escape characters
            //    $" " = an interpolated string, can include variables
            string s1 = "Hello there everyone. File path: C:\\Users\\Dude\\file.txt";
            string s2 = @"Hello there everyone. File path: C:\Users\Dude\file.txt";
            string s3 = $"I like adding strings together {s1}";
            string s4 = @"This is a long sentence
that will go over multiple lines
and print this way.";

            #endregion

            #region "2) Accessing characters, substring and length"

            // Use square brackets [n] to read individual characters.
            // The number is called an index. The index must access a
            // character that exists, otherwise you'll raise a runtime
            // error.
            char firstCharacter = userSentence[0];
            Console.WriteLine(firstCharacter);

            // Substring() can be used to read part of a string. You can
            // produce errors if you try to access characters past the end
            // of the string.
            // Written as: Substring(start, length)
            string firstFiveCharacters = userSentence.Substring(0, 5);
            Console.WriteLine(firstFiveCharacters);

            // Finally you can read the length of strings by using the 
            // length property.
            Console.WriteLine(userSentence.Length);

            #endregion

            #region "3) Contains, ToUpper and ToLower"

            // You can also use methods to look for words inside a string
            Console.WriteLine("String s1 contains the word there: " + userSentence.Contains("there"));

            // You can convert strings to uppercase or lowercase.
            Console.WriteLine(userSentence.ToLower());
            Console.WriteLine(userSentence.ToUpper());

            #endregion

            #region "4) Replace and IndexOf"

            // Use the Replace() method to replace characters or 
            // substrings with something else.
            string replacedString = userSentence.Replace("e", "b");
            Console.WriteLine($"Your string where e's are now b's: {replacedString}");

            // Use the IndexOf method 
            int index = userSentence.IndexOf("a");
            Console.WriteLine($"The letter 'a' starts at index: {index}");

            #endregion

        }

    }
}

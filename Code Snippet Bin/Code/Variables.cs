﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class Variables
    {
        /// <summary>
        /// Variables are one of the most important building blocks for 
        /// programming. They allow you to store, collect and remember data.
        /// </summary>
        public static void Run()
        {

            // IGNORE the green squiggly lines, they are warnings telling me
            // that I'm not using those variables.

            #region "1) Creating variables"

            // var keyword allows you to create a variable with a name 
            // and data (that determines the variable's datatype). Variable 
            // names can only have numbers, letters and underscores. 
            // Variable names must also start NOT start with a number.
            // Creating a variable is known as delcaring.
            var mark = 10;
            var fractionalNumber = 3.14;
            var firstName = "Bob";
            var passedExam = false;

            // If you know WHAT data will be stored but NOT the value itself
            // you can replace the var keyword with one of the following:
            //    int     - stores whole numbers, short for integer
            //    float   - stores fractional numbers, short for single floating point number
            //    string  - alphanumeric text, special characters 
            //    bool    - true or false
            int mark2;
            float fractionalNumber2;
            string firstName2;
            bool passedExam2;

            #endregion

            #region "2) Arithmetic operations"

            // Arithmetic operators allow you to perform maths functions
            // on integers and floats. They include:
            //    + add
            //    - subtract
            //    * times
            //    / divide
            //    % modulus
            //    ( ) order of operation
            var number1 = 10;
            var number2 = 20;
            var number3 = number1 + number2;
            var number4 = (number3 - number2) * number1;

            #endregion

            #region "3) More maths functions"

            // Math functions allow you to perform more complex maths
            // functions on integers and floats.
            //    Math.Pow() calculates to the power of
            //    Math.Abs() returns the absolute value 
            //    Math.Sin() performs the sine operation
            //    Math.Cos() performs the cosine operation
            //    Math.Tan() performs the tan operation
            //    Math.Ceiling() rounds a fractional number down to the nearest whole number
            //    Math.Floor() rounds a fractional number up to the nearest whole number
            var number5 = 1.6;
            var number6 = 5;
            var number7 = -67;

            var pow = Math.Pow(number5, number6);
            var abs = Math.Abs(number7);
            var sin = Math.Sin(number5);
            var cos = Math.Cos(number5);
            var tan = Math.Tan(number5);
            var ceiling = Math.Ceiling(number5);
            var floor = Math.Floor(number5);

            // Reading variables can be done with the WriteLine 
            // function without quotes or using $"{string interpolation}"
            Console.WriteLine("Result of Math.Pow(1.6, 5) was " + pow);
            Console.WriteLine("Result of Math.Ceiling(1.6) was " + ceiling + " and Math.Floor(1.6) was " + floor);
            Console.WriteLine($"Result of Math.Abs(-67) was {abs}");
            Console.WriteLine($"Result of Math.Sin(1.6) was {sin}, Math.Cos(1.6) was {cos} and Math.Tan(1.6) was {tan}");

            #endregion

        }

    }
}

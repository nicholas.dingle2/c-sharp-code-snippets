﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{

    static class Lists
    {

        /// <summary>
        /// A list is a special datatype that allows a programmer to store 
        /// multiple variables at the same time. An index can be used to access
        /// individual variables. Provides methods to search, sort, and 
        /// manipulate items.
        /// 
        /// Can store any variable datatype.
        /// </summary>
        public static void Run()
        {

            #region "1) Creating a list"

            // List<> is used around a datatype to create a list. The 
            // = new List<string>() tells C# to allocate memory.
            List<string> stringList = new List<string>();
            List<int> intList = new List<int>();

            // !!!!This will cause a runtime error if used!!!!
            List<float> floatList;

            #endregion

            #region "2) Adding items"

            // Create a list for names
            List<string> names = new List<string>();

            // Use Add() with the correct variable datatype to add
            // a new item to the end of the list
            names.Add("Bob");
            names.Add("Jill");
            names.Add("Peter");
            names.Add("Mary");

            #endregion

            #region "3) Adding lists together"

            List<string> newNames = new List<string>();
            newNames.Add("John");
            newNames.Add("Hillary");
            newNames.Add("Robert");
            newNames.Add("Kimberley");

            // Use AddRange() to add multiple items at once, both lists 
            // MUST be of the same datatype.
            names.AddRange(newNames);

            #endregion

            #region "4) Looping through items"

            // Use a foreach ( var item in list) to read a list of items.
            // The loop will start with the first item of the list and 
            // on the next repeat will move to the next item. Loop stops
            // when all items have been read once.
            foreach (var item in names)
            {
                Console.WriteLine(item);
            }

            #endregion

            #region "5) Removing items"

            // Use RemoveAt() to remove an item in a specific position.
            // RemoveAt(2) will remove the third item in the list.
            names.RemoveAt(2);

            // Use Remove() to specify a data value to remove. 
            // Remove("Hillary") will remove the first instance of Hillary.
            names.Remove("Hillary");

            #endregion

            #region "6) User Input and List Functions"

            // Create the list ready for the users number
            List<int> numbers = new List<int>();
            int userNumber = -1;

            // Setup a loop
            while (userNumber != 0)
            {

                // Get a number
                Console.Write("Please enter a number (0 or negative to stop): ");
                userNumber = int.Parse(Console.ReadLine());

                // Did they enter a positive number
                if (userNumber > 0)
                {
                    numbers.Add(userNumber);
                }

            }

            // Give them some details
            Console.WriteLine("Here are some of your details:");
            Console.WriteLine($" * Number count - {numbers.Count}");
            Console.WriteLine($" * Your first number - {numbers.First()}");
            Console.WriteLine($" * Your last number - {numbers.Last()}");
            Console.WriteLine($" * Numbers total - {numbers.Sum()}");
            Console.WriteLine($" * Numbers average - {numbers.Average()}");
            Console.WriteLine($" * The smallest number - {numbers.Min()}");
            Console.WriteLine($" * The biggest number - {numbers.Max()}");

            #endregion

            #region "7) Sort, IndexOf"

            // You can perform searches on lists
            var indexOf1 = numbers.IndexOf(1);
            var indexOf7 = numbers.IndexOf(7);

            // Print the result
            Console.WriteLine($"The position of the number 1 is {indexOf1}");
            Console.WriteLine($"The position of the number 7 is {indexOf7}");

            // When you have a list, you can perform lots of operations by default,
            // lists work really well with numbers. 
            numbers.Sort();

            Console.WriteLine("Here are your numbers, but sorted:");

            // Print the sorted numbers
            foreach (var number in numbers)
            {
                Console.WriteLine($" {number}");
            }

            #endregion

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class UserInput
    {

        /// <summary>
        /// Programs are boring if users aren't able to interact. User
        /// input allows them to enter data and make choices.
        /// </summary>
        public static void Run()
        {

            #region "1) Reading user data"

            // Variables should be used to store values entered by the user.
            // A good rule of thumb for how many variables you need "How many
            // values does the user enter?".
            string username;
            string password;


            // Console.ReadLine() is used to get a string from the user. It
            // allows the user to type in text and when they press enter, the
            // result is stored in your variable on the left.
            Console.Write("What is your username? ");
            username = Console.ReadLine();
            Console.Write("What is your password? ");
            password = Console.ReadLine();


            // Just like the variables example, we can print varaible values
            // with WriteLine
            Console.WriteLine($"Welcome {username}, your password \"{password}\" has been saved... not really.");

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "2) Reading numbers"

            // int.Parse() is required to read numbers, this is because
            // ReadLine returns a string. So we text the string and convert 
            // it into an integer. 
            //      e.g. int number = "66" is illegal because the quotes make it the word 66
            // 
            // NOTE: Will raise a runtime error if a word is entered
            int number1;
            int number2;
            Console.Write("Give me a number, please: ");
            number1 = int.Parse(Console.ReadLine());
            Console.Write("Give me another number, please: ");
            number2 = int.Parse(Console.ReadLine());

            Console.WriteLine($"{number1} + {number2} = {number1 + number2}");

            #endregion

            #region "3) Reading fractional numbers"

            // float.Parse() is required to read fractional numbers, this
            // again is because ReadLine provides a string and not a float.
            // So we take the string and convert it to the a single floating
            // point number.
            // 
            // NOTE: Will raise a runtime error if a word is entered
            float fractional;
            Console.Write("Enter a fractional/huge number number: ");
            fractional = float.Parse(Console.ReadLine());

            Console.WriteLine($"Thank you for the {fractional} number");

            // Pause
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();

            #endregion

            #region "4) User interacting with commands"

            // Input can be used for more than just printing it back on the 
            // screen or adding it. 
            Console.Write("What's your favourite colour from 0 to 15? ");
            int colour = int.Parse(Console.ReadLine());

            // (ConsoleColor) converts the number given into the correct colour.
            Console.BackgroundColor = (ConsoleColor)colour;
            Console.WriteLine("Nice colour!");

            #endregion

        }

    }
}

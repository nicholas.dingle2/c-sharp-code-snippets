﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class TextFiles
    {

        /// <summary>
        /// Text files are the most basic form of storing data as it is plain text. They
        /// allows you to store your program's data for later use. This is similar to saving
        /// a document in Microsoft Word.
        /// 
        /// When working with files, you need to remember the 3 steps:
        ///  1) Open file
        ///  2) Do stuff
        ///  3) Close file
        ///  
        /// Files can be opened in different modes:
        ///  1) Write
        ///  2) Read
        ///  3) Append
        ///  
        /// Files use a new namespace (using System.IO) on line 3 ABOVE to enable the file functionality.
        /// </summary>
        public static void Run()
        {

            // NOTE: All the files created in these examples will be found inside your project's
            // debug folder. i.e. "Code Snippet Bin\bin\Debug"

            #region "1) Opening and closing a file"

            // When opening a file, you must specify the file's name and what mode
            // you want use (Read, Write, Append)

            // The following line will create an empty text file called myFile.txt inside the project's
            // folder Debug. The myFile variable name doesn't need to match the filename.
            var myFile = File.CreateText("myFile.txt");

            // After you are finished with a file, ensure you close it!
            myFile.Close();

            #endregion

            #region "2) Writing into files"

            // Create and open the file first, if the file already exists, it will be wiped first.
            var fullFile = File.CreateText("fullFile.txt");

            // Write into the file
            fullFile.WriteLine("This is inside the file");
            fullFile.WriteLine("And this is the second line");
            fullFile.WriteLine("Ok, I'm done");

            // Close the file
            fullFile.Close();

            #endregion

            #region "3) Appending to a file"

            // Appending is the idea of adding to an existing file
            fullFile = File.AppendText("fullFile.txt");

            // Append some text to it
            fullFile.WriteLine("I lied, I wasn't finished");
            fullFile.WriteLine("Why not?!");
            fullFile.WriteLine("Have fun programming");

            // Close it
            fullFile.Close();

            #endregion

            #region "4) Reading from files"

            // Firstly, files MUST exist before you try to open them. Otherwise, a runtime error
            // will occur and crash your program. The best way to avoid this is to check if the
            // file exists first.

            // File.Exists() will return true if the file exists
            if (File.Exists("fullFile.txt"))
            {

                // Open the file for reading
                var file = File.OpenText("fullFile.txt");

                // Use ReadLine() to read file contents, each ReadLine will move to the next line
                var firstLine = file.ReadLine();
                var secondLine = file.ReadLine();

                Console.WriteLine($"First line of the file was '{firstLine}'");
                Console.WriteLine($"Second line of the file was '{secondLine}'");

                // Close
                file.Close();

            }

            #endregion

            #region "5) Reading whole file"

            // File.Exists() will return true if the file exists
            if (File.Exists("fullFile.txt"))
            {

                // Open the file for reading
                var file = File.OpenText("fullFile.txt");

                // Tell them
                Console.WriteLine("This is the full file: ");

                // Use a while loop to continue reading
                while (file.EndOfStream == false)
                {
                    var line = file.ReadLine();
                    Console.WriteLine(line);
                }

                // Close
                file.Close();

            }

            #endregion

        }

    }
}

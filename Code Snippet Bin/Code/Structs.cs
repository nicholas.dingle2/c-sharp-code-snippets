﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code_Snippet_Bin
{
    static class Structs
    {

        #region "1) Creating a struct"

        // struct is the keyword followed by the name of your datatype to create a new struct.
        // Inside the following code block, the properties/members of the struct are defined.
        struct Person
        {
            public string Firstname;
            public string Lastname;
            public int Age;
            public float Height;
        }

        struct Book
        {
            public float Price;
            public string Title;
            public string Author;
        }

        #endregion

        /// <summary>
        /// Structs are a way to make your own datatypes, alongside your strings, integers and more.
        /// A list is used to store multiple variables of the same datatype (homogeneous) and structs
        /// are used to store multiple variables of different datatypes (heterogeneous). 
        /// 
        /// When you need to store data about a real world object, such as a person, object, place
        /// then use a struct. FIRST EXAMPLE is up above.
        /// </summary>
        public static void Run()
        {

            #region "2) Use a struct"

            // After you make a struct, you must create a variable of that datatype in order to
            // use it. Then use the dot (.) to set the values of the members.
            Person person1;

            person1.Firstname = "Bobby";
            person1.Lastname = "Bob";
            person1.Age = 53;
            person1.Height = 163;

            Book book1;

            book1.Price = 35.6f;
            book1.Title = "The Zen of Farting";
            book1.Author = "Carl Japikse";

            // Just like a variable, you can read values
            Console.WriteLine($"The book {book1.Title} will cost you ${book1.Price} and was written by {book1.Author}");

            #endregion

            #region "3) Using user input"

            // ReadLine() can be used to enter values into a struct
            Person person2;

            Console.Write("What's your first name? ");
            person2.Firstname = Console.ReadLine();

            Console.Write("What's your last name? ");
            person2.Lastname = Console.ReadLine();

            Console.Write("What's your age? ");
            person2.Age = int.Parse(Console.ReadLine());

            Console.Write("What's your height? ");
            person2.Height = float.Parse(Console.ReadLine());

            // Reply back
            Console.WriteLine($"Hi {person2.Firstname} {person2.Lastname}, for {person2.Age} you look alright. However, {person2.Height} is short...");

            #endregion

            #region "4) Structs and lists!!!"

            // Lists are incredibly handy to store multiple structs, start by creating the list
            List<Book> bookShelf = new List<Book>();

            // I use this variable to tell us if we want to repeat
            bool repeat = true;

            // Let's setup a do while loop 
            do
            {
                // Create a new struct variable and fill it
                Book newBook;

                Console.Write("Enter the book's price: ");
                newBook.Price = float.Parse(Console.ReadLine());

                Console.Write("Enter the book's title: ");
                newBook.Title = Console.ReadLine();

                Console.Write("Enter the book's author: ");
                newBook.Author = Console.ReadLine();

                // Now add it to the list
                bookShelf.Add(newBook);

                // Do they want to continue?
                Console.Write("Do you want to add another book (y/n)? ");
                string yesNo = Console.ReadLine();

                if (yesNo == "no")
                {
                    repeat = false;
                }

            } while (repeat == true);

            // Since we have a list, let's split out some details
            Console.WriteLine("You bookshelf deails are:");
            Console.WriteLine($" * Book count: {bookShelf.Count}");
            Console.WriteLine($" * First book's title: {bookShelf[0].Title}");
            Console.WriteLine($" * First book's author: {bookShelf[0].Author}");
            Console.WriteLine($" * First book's price: {bookShelf[0].Price}");

            #endregion

        }

    }
}
